const path = require("path");

module.exports = {
  // NOTE: Regarding "eval" (https://github.com/webpack/webpack/issues/5627)
  devtool: 'inline-source-map',
  mode: 'development',
  entry: {
    popup: "./src/popup.js",
    content: "./src/content.js"
  },
  output: {
    path: path.resolve(__dirname, "ext_src"),
    filename: "[name]/index.js"
  },
  module: {
    rules: [
      {
        enforce: "pre",
        test: /\.js$/,
        include: [ path.join(__dirname, 'src') ],
        loader: 'eslint-loader'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [ path.join(__dirname, 'src') ],
        options: {
          cacheDirectory: true,
          presets: ['@babel/preset-react'],
        },
      },
      {
        test: /\.scss$/,
        use: [ 'style-loader', 'css-loader', 'sass-loader' ]
      }
    ]
  },
};
