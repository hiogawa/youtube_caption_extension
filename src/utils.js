const fetchText = (...args) => fetch(...args).then(resp => {
  if (!resp.ok) { throw null; }
  return resp.text();
});

const ExtractSubtitleInfo = (content) => {
  var mobj = content.match(/;ytplayer\.config\s*=\s*({.+?});ytplayer/);
  var config = JSON.parse(mobj[1]);
  var player_response = JSON.parse(config.args.player_response);
  var tracks = player_response.captions.playerCaptionsTracklistRenderer.captionTracks;
  var translations = player_response.captions.playerCaptionsTracklistRenderer.translationLanguages;
  return {
    tracks: tracks.map(track => ({
      name: track.name.simpleText,
      url: track.baseUrl + "&fmt=ttml"
    })),
    translations: translations.map(translation => ({
      name: translation.languageName.simpleText,
      languageCode: translation.languageCode
    }))
  };
}

const ExtractSubtitle = (content) => {
  var doc = (new DOMParser()).parseFromString(content, 'text/xml');
  // NOTE: Handle subtitle containing "<p ...> ... <br /> ... </p>"
  Array.from(doc.getElementsByTagName('br')).forEach(br => br.replaceWith(' '));
  var ps = doc.getElementsByTagName('p');
  return Array.from(ps).map((p) => ({
      begin: p.attributes['begin'].value,
      end: p.attributes['end'].value,
      text: p.textContent
  }));
}

export const getSubtitleInfo = (videoId) =>
  fetchText(`https://www.youtube.com/watch?v=${videoId}`, { headers: { 'Accept-Language': 'en-US,en' } })
  .then(ExtractSubtitleInfo)

export const getEntries = (ttmlUrl) =>
  fetchText(ttmlUrl)
  .then(ExtractSubtitle)
