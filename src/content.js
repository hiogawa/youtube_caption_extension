import browser from 'webextension-polyfill';
import _ from 'lodash';

let host = document.querySelector('#caption-extension-root');
if (!host) {
  host = document.createElement('div');
  host.setAttribute('id', 'caption-extension-root');
  host.attachShadow({ mode: 'open' });
  document.body.appendChild(host);
}

browser.runtime.onMessage.addListener((message) => {
  if (message.command === 'showEntries') {
    host.shadowRoot.innerHTML = _.template(`
      <style>
        #panel {
          width: 32%;
          height: 85%;
          top: 8%;
          right: 0;
          position: absolute;
          z-index: 10000;

          overflow-y: auto;
          padding: 10px;
          background-color: rgba(210, 210, 210);
        }
        .entry {
          font-size: 13px;
          line-height: 15px;
          color: black;
          background-color: white;
          border-radius: 3px;
          box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.2),
                      0 1px 2px rgba(0, 0, 0, 0.2);
          padding: 3px 7px 6px 7px;
          margin-bottom: 4px;
        }
        .entry__header {
          display: flex;
          flex-direction: row;
          align-items: center;
          margin: 1px 0px -1px 0px;
          color: grey;
          font-size: 12px;
        }
        .entry__body {
          display: flex;
          flex-direction: row;
          overflow-x: auto;
          overflow-y: hidden;
        }
        .entry__body > div {
          flex: 1 1 50%;
        }
        .entry__body > div:first-child {
          padding-right: 7px;
          border-right: 1px solid grey;
        }
        .entry__body > div:not(:first-child) {
          padding-left: 7px;
        }
      </style>
      <div id='panel'>
        <% _.forEach(entries, function(entry) { %>
          <div class='entry'>
            <div class='entry__header'><%= entry.begin %> - <%= entry.end %></div>
            <div class='entry__body'>
              <div><%= entry.text1 %></div>
              <div><%= entry.text2 %></div>
            </div>
          </div>
        <% }); %>
      </div>
    `)({ entries: message.payload });
  }
});
