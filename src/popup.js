import './popup.scss';
import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import browser from 'webextension-polyfill';
import _ from 'lodash';
import { getSubtitleInfo, getEntries } from './utils.js';

class Popup extends Component {
    constructor(props) {
        super(props);
        this.state = {
          videoId: '',
          subtitleInfo: {
            tracks: [],
            translations: []
          },
          url1: '',
          url2: '',
          subtitleInfoLoading: true,
          entriesLoading: false,
        };

        browser.tabs.query({ active: true, currentWindow: true }).then(tabs => {
          const url = new URL(tabs[0].url);
          const videoId = url.searchParams.get('v');
          if (url.host == 'www.youtube.com' && videoId) {
            this.setState({ videoId });
            getSubtitleInfo(videoId).then(val => {
              this.setState({
                subtitleInfoLoading: false,
                subtitleInfo: val,
                url1: val.tracks.length > 0 ? val.tracks[0].url : '',
                url2: val.tracks.length > 0 ? val.tracks[0].url : ''
              });
            });
          }
        });
    }
    showCaptions() {
      const { url1, url2 } = this.state;
      this.setState({ entriesLoading: true });
      Promise.all([getEntries(url1), getEntries(url2)]).then(([entries1, entries2]) => {
        this.setState({ entriesLoading: false });
        const entries = _.zipWith(entries1, entries2, (entry1, entry2) => ({
          text1: entry1.text,
          text2: entry2.text,
          begin: entry1.begin,
          end:   entry1.end,
        }));
        browser.tabs.executeScript({file: "/content/index.js"}).then(() => {
          browser.tabs.query({ active: true, currentWindow: true }).then(tabs => {
            browser.tabs.sendMessage(tabs[0].id, {
              command: 'showEntries',
              payload: entries
            });
          });
        });
      });
    }
    render() {
      const { videoId, subtitleInfo, url1, url2, subtitleInfoLoading, entriesLoading } = this.state;
      const selectDisabled = subtitleInfo.tracks.length == 0;

      if (subtitleInfoLoading) {
        return 'Loading...';
      }
      return (
        <div>
          <div>Video ID: { videoId }</div>
          <div>
            <select disabled={selectDisabled} value={url1}
                onChange={(e) => this.setState({ url1: e.target.value, entries1: [], entries2: [] })}>
              {
                subtitleInfo.tracks.map(track =>
                  <Fragment key={track.name}>
                    <option key={track.name} value={track.url}>
                        { track.name }
                    </option>
                  </Fragment>
                )
              }
              {
                subtitleInfo.tracks.map(track =>
                  <Fragment key={track.name}>
                    <optgroup label={`-- Auto-translations from ${track.name} --`}>
                      {
                        subtitleInfo.translations.map(t =>
                          <option key={t.languageCode} value={`${track.url}&tlang=${t.languageCode}`}>
                            { t.name }
                          </option>
                        )
                      }
                    </optgroup>
                  </Fragment>
                )
              }
            </select>
            <select disabled={selectDisabled} value={url2}
                onChange={(e) => this.setState({ url2: e.target.value, entries1: [], entries2: [] })}>
              {
                subtitleInfo.tracks.map(track =>
                  <Fragment key={track.name}>
                    <option key={track.name} value={track.url}>
                        { track.name }
                    </option>
                  </Fragment>
                )
              }
              {
                subtitleInfo.tracks.map(track =>
                  <Fragment key={track.name}>
                    <optgroup label={`-- Auto-translations from ${track.name} --`}>
                      {
                        subtitleInfo.translations.map(t =>
                          <option key={t.languageCode} value={`${track.url}&tlang=${t.languageCode}`}>
                            { t.name }
                          </option>
                        )
                      }
                    </optgroup>
                  </Fragment>
                )
              }
            </select>
          </div>
          <button onClick={() => this.showCaptions()} >
            { entriesLoading ? 'Loading...' : 'Show Caption Panel' }
          </button>
        </div>
      );
    }
}

ReactDOM.render(<Popup />, document.getElementById('app'));
